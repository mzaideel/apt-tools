#!/bin/bash
#
# Copyright 2016 Zaideel.Com, Inc.
# All Rights Reserved.
# Authored by Mzaideel <m@zaideel.com>
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# Creating a tempfile directory to use later maybe
tempdir=$(mktemp -d )  || { echo "Failed to create temp file"; exit 1; }
line="---------------------------------------------------"

# If I need tempfiles lets do it cleanly 
function cleanup {
  echo ${line}
  cat ${tempdir}/*
  echo ${line}
  rm -rf ${tempdir} 
  echo "Removed ${tempdir}"
  exit
}

# Catch the crtl-c and others nicely 
trap cleanup  EXIT SIGHUP SIGINT SIGTERM
# 

# Wanted to output a nicer message while I debug things.
print(){
 echo "$1"
 echo "${line}"
}


# Using an associative array 
declare -A MIRROR

# To add another mirror just follow the format below
MIRROR[ubuntu]="http://us.archive.ubuntu.com/ubuntu trusty main restricted universe"
MIRROR[ubuntu-security]="http://us.archive.ubuntu.com/ubuntu trusty-security main restricted universe"
MIRROR[ubuntu-updates]="http://us.archive.ubuntu.com/ubuntu trusty-updates main restricted universe"

## Adding some other repos
MIRROR[salt]="http://ppa.launchpad.net/saltstack/salt/ubuntu trusty main"
MIRROR[docker]="http://get.docker.io/ubuntu docker main"
MIRROR[percona]="http://repo.percona.com/apt trusty main"
MIRROR[openstack-icehouse]="ppa:openstack-ubuntu-testing/icehouse"
MIRROR[ansible]="ppa:rquillo/ansible"

# This will create the mirror the first time if its not already on the machine.
create_mirror(){
  # Timestamp
  date > ${tempdir}/start
  echo -n "Start:  "
  cat ${tempdir}/start

  # Just wanting a list of mirrors before I started
  aptly mirror list > ${tempdir}/mirror-list  
  
  for mirror in "${!MIRROR[@]}"; do 
    if ! aptly mirror show $mirror > /dev/null ; then 
      print "aptly mirror create $mirror ${MIRROR[$mirror]}"
      aptly mirror create $mirror ${MIRROR[$mirror]} 
    fi
  done

  # Timestamp for just to see how long it takes
  echo -n "Finish: "
  date > ${tempdir}/end
  cat ${tempdir}/end
}

# Updating the mirrors and creates a snapshot for the day. Then publishes the mirror. 
# The publishing of a new snapshot requires dropping the already published. 
update_mirror(){
  for mirror in "${!MIRROR[@]}"; do 
    print "aptly mirror update $mirror"
    aptly mirror update $mirror 

    if ! aptly snapshot show ${mirror}-$(date +%Y%m%d) > /dev/null ; then
      print "aptly snapshot create ${mirror}-$(date +%Y%m%d) from $mirror"
      aptly snapshot create ${mirror}-$(date +%Y%m%d) from mirror $mirror
    fi
    # Need some logic here how snapshots are published over an already pubished one.
    # This will be moved to another function later.
    print "aptly publish snapshot ${mirror}-$(date +%Y%m%d) ${mirror}"
    aptly publish snapshot ${mirror}-$(date +%Y%m%d) ${mirror} 
  done
}

# Do things here.
create_mirror
update_mirror

# Clean up the aptly db of dangling references and packages nolonger used in the repos or 
# snapshots
aptly db cleanup

# vim: tabstop=2:softtabstop=2:shiftwidth=2:noexpandtab


